import { Component, OnInit,ViewChild,TemplateRef,Inject } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { NgProgress,NgProgressRef } from '@ngx-progressbar/core'
import {environment} from '../../../environments/environment'
import { DefaultLayoutComponent } from '../../containers/default-layout/default-layout.component'
import { ChildListenerService } from '../../views/Services/ChildListener.service';
import { Router } from '@angular/router';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Component({
  selector: 'app-SubCategory',
  templateUrl: './SubCategory.component.html',
  styleUrls: ['./SubCategory.component.scss']
})
export class SubCategoryComponent implements OnInit {
  url=environment.apiUrl;

  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Token ' + localStorage.getItem('token') });
  options = { headers: this.headers };
  
  checkedAllModel;
  newSubCategory;
  subCategoryList=[];
  subCategoryDetailList=[];

  subCategoryNameModel;
  keywordNameModel;
  subCategoryFromModel;
  subCategoryToModel;

  keywordList=[];
  alerts=[];

  modalRef: BsModalRef;
  pointerArray;
  pointerKeyword;
  editSubCategory;

  deleteType;
  @ViewChild('subCategoryDetail') public subCategoryDetail;  
  @ViewChild('editNameSubCategory') public editNameSubCategory;  

  constructor(@Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent,private http: HttpClient,public progress: NgProgress,private router: Router,private modalService: BsModalService) { }

  updateSubCategory(client){
    this.pointerArray = client;
    this.editSubCategory = client.category_id;
    this.editNameSubCategory.show();
  }

  doUpdateSubCategory(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let param = {"category_id":this.pointerArray.category_id, "new_category_id":this.editSubCategory}
    this.http.post<UserResponse>(this.url + 'user/sub-category/update',param,this.options)
    .subscribe((result: any) => {
      for (let i=0;i<this.subCategoryList.length;i++){
        if (this.subCategoryList[i].category_id== this.pointerArray.category_id){
          this.subCategoryList[i].category_id = result.data.category_id;
        }
      }
      this.editNameSubCategory.hide();
      this.editSubCategory ='';
      this.alerts=[];
      this.alerts.push({'type':'success','msg':'Sub Media has been updated','timeout':3000}) 
      
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.editNameSubCategory.hide();
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while saving data.','timeout':3000}) 
      this.parent.progressRef.complete();
    }); 
  }

  loadGroupCategory(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    //group category
    this.http.get<UserResponse>(this.url + 'user/categorycollections/',this.options)
      .subscribe((result: any) => {
        this.subCategoryList =  result.results.map(o => {
          return {'category_id':o.category_id};
        });        
        this.parent.progressRef.complete();
      },(err:any)=>{
        this.alerts=[];
        this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
        this.parent.progressRef.complete();
      });   
  }

  openModal(template: TemplateRef<any>,client,deltype) {
    if (deltype == 'subCategory'){
      this.pointerArray = client;
    }else{
      this.pointerKeyword = client;
    }
    
    this.deleteType = deltype;
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

  decline(): void {
    this.modalRef.hide();
  }

  doDeleteSubCategory(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let param;
    let urlPost;
    if (this.deleteType == 'subCategory'){
      param = {"category_id": this.pointerArray.category_id};
      urlPost = this.url + 'user/sub-category/delete';
    }else{
      param = {"category_id": this.pointerArray.category_id,"keyword":this.pointerKeyword.keyword};
      urlPost = this.url + 'user/keyword/delete';
    }

    // console.log('param nya : ' + JSON.stringify(param));
    // console.log('url nya ' + urlPost);
    // this.modalRef.hide();
    // this.parent.progressRef.complete();
    
    this.http.post<UserResponse>(urlPost,param,this.options)
    .subscribe((result: any) => {
      if (this.deleteType == 'subCategory'){
        for (let i=0;i<this.subCategoryList.length;i++){
          if (this.subCategoryList[i].category_id== result.data.category_id){
            this.subCategoryList.splice(i,1);
          }
        }
        this.alerts=[];
        this.alerts.push({'type':'success','msg':'Sub Category has been deleted','timeout':3000})   
      }else{
        for (let i=0;i<this.subCategoryDetailList.length;i++){
          if (this.subCategoryDetailList[i].keyword== result.data.keyword){
            this.subCategoryDetailList.splice(i,1);
          }
        }
        this.alerts=[];
        this.alerts.push({'type':'success','msg':'Keyword has been deleted','timeout':3000})   
      }
      this.modalRef.hide(); 
      
      
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.modalRef.hide(); 
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
      this.parent.progressRef.complete();
    }); 
  }

  showDetails(client){        
    this.parent.progressRef.start();
    this.pointerArray = client;
    // console.log(JSON.stringify(client))
    interface UserResponse {
      data: Object;
    }
    let param = {"category_id": client.category_id}
    this.http.post<UserResponse>(this.url + 'user/keywords/',param,this.options)
    .subscribe((result: any) => {
      this.subCategoryDetailList =[];
      this.subCategoryDetailList = result.data.map(o => {
        return {'keyword':o};
      });     
      
      this.parent.progressRef.complete();
      this.subCategoryDetail.show();
    },(err:any)=>{
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
      this.parent.progressRef.complete();
    });   
            
  }
  
  addNewSubCategory(){
   this.parent.progressRef.start();   
   interface UserResponse {
     data: Object;
   }
   let param = {"category_id": this.newSubCategory}
   this.http.post<UserResponse>(this.url + 'user/create/subcategory',param,this.options)
   .subscribe((result: any) => {
     this.subCategoryList.push(result.data);
     this.newSubCategory ='';
     this.alerts=[];
     this.alerts.push({'type':'success','msg':'Sub Category has been created','timeout':3000}) 
     
     this.parent.progressRef.complete();
   },(err:any)=>{
     this.alerts=[];
     this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
     this.parent.progressRef.complete();
   }); 
  }

  addKeyword(){
    this.parent.progressRef.start();   
   interface UserResponse {
     data: Object;
   }
   let param = {
    "category_id": this.pointerArray.category_id, 
    "keyword":this.keywordNameModel, 
    "start_date": (moment(this.subCategoryFromModel).format('YYYY-MM-DD')).toString(), 
    "end_date": (moment(this.subCategoryToModel).format('YYYY-MM-DD')).toString()
  }
   this.http.post<UserResponse>(this.url + 'user/keyword/create',param,this.options)
   .subscribe((result: any) => {
     this.subCategoryDetailList.push(result.data);

     this.alerts=[];
     this.alerts.push({'type':'success','msg':'Keyword has been created','timeout':3000}) 
     
     this.parent.progressRef.complete();
   },(err:any)=>{
     this.subCategoryDetail.hide();
     this.alerts=[];
     this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
     this.parent.progressRef.complete();
   }); 
  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

  ngOnInit() {
    this.loadGroupCategory();
  }
}
