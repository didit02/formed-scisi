import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubCategoryComponent } from './SubCategory.component';

const routes: Routes = [
  {
    path: '',
    component: SubCategoryComponent,
    data: {
      title: 'Sub Category'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubCategoryRoutingModule {}
