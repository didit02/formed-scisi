import {
  Component,
  OnInit,
  ViewChild,
  Inject,
  TemplateRef
} from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { NgProgress, NgProgressRef } from "@ngx-progressbar/core";
import { environment } from "../../../environments/environment";
import { DefaultLayoutComponent } from "../../containers/default-layout/default-layout.component";
import { ChildListenerService } from "../../views/Services/ChildListener.service";
import { Router } from "@angular/router";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { AlertComponent } from "ngx-bootstrap/alert/alert.component";
import { NONE_TYPE } from "@angular/compiler/src/output/output_ast";
import { Subscription } from "rxjs";
import { moment } from "ngx-bootstrap/chronos/test/chain";
import { ListenerService } from "../Services/listener.service";
import * as $ from "jquery";
import { element } from "protractor";
import { elementEnd } from "@angular/core/src/render3/instructions";
@Component({
  selector: "app-Influencer",
  templateUrl: "./Influencer.component.html",
  styleUrls: ["./Influencer.component.scss"]
})
export class InfluencerComponent implements OnInit {
  url = environment.apiUrl;
  alerts;
  headers = new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: "Token " + localStorage.getItem("token")
  });
  options = { headers: this.headers };
  modalRef: BsModalRef;
  InfluencerConfList = [];
  InfluencerDetailsList = [];
  influencerkeywordModel;
  influencerModel;
  backtrackModel: Date = null;
  influencer_id;
  editInfluence: boolean = false;
  paramsMap: any;
  keywordNameModel;
  subscription: Subscription;
  contentinfluencer: string;
  subCategoryDetailList=[];
  influencerBacktrackinvalid:boolean = false;
  dataold = [];
  constructor(
    @Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent,
    private http: HttpClient,
    public progress: NgProgress,
    private childListener: ChildListenerService,
    private router: Router,
    private modalService: BsModalService,
    private listener: ListenerService
  ) {}
  @ViewChild("influencerDetailsModals") public influencerDetailsModals;
  @ViewChild("influencerModals") public influencerModals;
  @ViewChild('subCategoryDetail') public subCategoryDetail;  

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

  loadInfluencer() {
    interface UserResponse {
      result: Object;
    }
    this.http
      .get<UserResponse>(
        environment.apiInfluencer+"influencer-list/",
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          this.InfluencerConfList = result.data;
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
        }
      );
  }

  influencerDetails(spokeperson) {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    this.influencer_id = spokeperson.influencer_id;
    this.http
      .get<UserResponse>(
        this.url + "user/influencer-details/" + this.influencer_id,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          this.InfluencerDetailsList = result.results;
          this.influencerDetailsModals.show();
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while loading data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
        }
      );
  }

  addkeyword() {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }

    let dataform = {
      influencer_id: this.influencer_id,
      influencer_details_key: this.influencerkeywordModel
    };
    this.http
      .post<UserResponse>(
        this.url + "user/influencer-details/create",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          this.InfluencerDetailsList.unshift(result.data);
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while saving data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
        }
      );
  }

  addInfluencer() {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      influencer_name: this.influencerModel
    };
    this.http
      .post<UserResponse>(
        environment.apiInfluencer+"create-influencer/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
          if (result["message"] === "success create influencer") {
            this.parent.progressRef.complete();
            this.InfluencerConfList.unshift(result.data);
            this.influencerModals.hide();
          } else {
            this.parent.progressRef.complete();
            this.influencerModals.hide();
            this.alerts = [];
            this.alerts.push({
              type: "danger",
              msg: result["message"],
              timeout: 3000
            });
          }
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while saving data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
          this.influencerModals.hide();
        }
      );
  }

  editInfluencer(influencer) {
    this.influencer_id = influencer.influencer_id;
    this.influencerModel = influencer.influencer_name;
    this.backtrackModel = influencer.influencer_backtrack;
    this.editInfluence = true;
    this.listener.eventinfluencer = "EDIT INFLUENCER";
    this.contentinfluencer = this.listener.eventinfluencer;
    $("#influencerName").prop("disabled", false);
    this.influencerModals.show();
    this.dataold.push(this.influencerModel);
    
  }

  updateInfluencer() {}

  ngOnInit() {
    this.paramsMap = {
      start_date: moment(this.parent.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.parent.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };
    this.loadInfluencer();
    this.subscription = this.childListener.notifyObservable$.subscribe(res => {
      if (res.hasOwnProperty("option") && res.option === "call_child") {
        this.paramsMap = {
          start_date: moment(this.parent.periodFromModel)
            .format("YYYY-MM-DD")
            .toString(),
          end_date: moment(this.parent.periodEndModel)
            .format("YYYY-MM-DD")
            .toString()
        };
        this.loadInfluencer();
      } else if (
        res.hasOwnProperty("option") &&
        res.option === "save_article"
      ) {
      }
    });
  }
  addInfluencerfunc() {
    this.influencerModel = undefined;
    this.listener.eventinfluencer = "ADD INFLUENCER";
    this.contentinfluencer = this.listener.eventinfluencer;
    $("#influencerName").prop("disabled", false);
    this.influencerModals.show();
  }
  editInfluencerfunc() {
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      influencer_name: this.dataold[0],
      new_influencer_name: this.influencerModel
    };
    this.http
      .post<UserResponse>(
        environment.apiInfluencer+"update-influencer/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          this.influencerModals.hide();
          this.loadInfluencer();
          this.dataold = []
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while saving data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
          this.influencerModals.hide();
        }
      );
  }
  deleteInfluencerfunc(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      influencer_name: this.influencerModel
    };
    this.http
      .post<UserResponse>(
        environment.apiInfluencer+"delete-influencer/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          this.influencerModals.hide();
          this.loadInfluencer();
          this.dataold = []
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while saving data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
          this.influencerModals.hide();
        }
      );
  }
  deleteInfluencer(influencer){
    this.listener.eventinfluencer = "DELETE INFLUENCER";
    $("#influencerName").prop("disabled", true);
    this.contentinfluencer = this.listener.eventinfluencer;
    this.influencerModel = influencer.influencer_name;
    this.influencerModals.show();
  }
  addInfluencerkeyword(influencer){
    this.influencerModel = influencer.influencer_name;
    this. getkeywordinfluencer();
  }
  addKeywordinfluencer(){    
    let time  = this.backtrackModel.toISOString().split('.')[0].replace('T' , ' ');
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      influencer_name: this.influencerModel,
      keyword:this.keywordNameModel,
      backtrack_date:time.split(' ')[0]
    };        
    let getdataform = {
      influencer_name: this.influencerModel,
    };    
    this.http
      .post<UserResponse>(
        environment.apiInfluencer+"create-keyword/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
          if (result['data']['message'] !== "success create influencer"){
            this.alerts = [];
              this.alerts.push({
                type: "danger",
                msg: result['data']['message'],
                timeout: 3000
              });
          }
          this.parent.progressRef.complete();
          this.http
          .post<UserResponse>(
            environment.apiInfluencer+"get-keyword-id/",
            getdataform,
            this.options
          )
          .subscribe(
            (result: any) => {
              this.parent.progressRef.complete();
              this.InfluencerDetailsList = []
              result['data'].forEach(element => {
                  element.data.forEach(elementdata => {
                  this.InfluencerDetailsList.push(elementdata)  
                  });
                  
              });  
            },
            (err: any) => {
              this.alerts = [];
              this.alerts.push({
                type: "danger",
                msg: "snap, error while saving data.",
                timeout: 3000
              });
              this.parent.progressRef.complete();
            }
          );                
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while saving data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
        }
      );
  }
  getkeywordinfluencer(){
    interface UserResponse {
      result: Object;
    }
    let dataform = {
      influencer_name: this.influencerModel,
    };    
    this.http
      .post<UserResponse>(
        environment.apiInfluencer+"get-keyword-id/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          this.InfluencerDetailsList = []
          result['data'].forEach(element => {
              element.data.forEach(elementdata => {
              this.InfluencerDetailsList.push(elementdata)  
              });
              
          });  
          this.subCategoryDetail.show(); 
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while saving data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
        }
      );
  }
  deletekeywordinfluencer(keyword){
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      influencer_name: this.influencerModel,
      keyword:keyword,
    };        
    let getdataform = {
      influencer_name: this.influencerModel,
    };    
    this.http
      .post<UserResponse>(
        environment.apiInfluencer+"delete-keyword/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          this.http
          .post<UserResponse>(
            environment.apiInfluencer+"get-keyword-id/",
            getdataform,
            this.options
          )
          .subscribe(
            (result: any) => {
              this.parent.progressRef.complete();
              this.InfluencerDetailsList = []
              result['data'].forEach(element => {
                  element.data.forEach(elementdata => {
                  this.InfluencerDetailsList.push(elementdata)  
                  });
                  
              });  
            },
            (err: any) => {
              this.alerts = [];
              this.alerts.push({
                type: "danger",
                msg: "snap, error while saving data.",
                timeout: 3000
              });
              this.parent.progressRef.complete();
            }
          );                
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while saving data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
        }
      );
  }
  createinfluencerkey(client){
    this.listener.eventinfluencer = "ADD INFLUENCER KEY";
    $("#influencerName").prop("disabled", true);
    this.contentinfluencer = this.listener.eventinfluencer;
    this.influencerModel = client.influencer_name;
    this.influencerModals.show();
  }

  addInfluencerkeyfunc(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      influencer_name: this.influencerModel
    };
    this.http
      .post<UserResponse>(
        environment.apiInfluencer+"add-influencer-key/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          this.influencerModals.hide();
          this.loadInfluencer();
          this.dataold = []
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while saving data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
          this.influencerModals.hide();
        }
      );
  }
  deleteInfluencerkey(client){
    this.listener.eventinfluencer = "DELETE INFLUENCER KEY";
    $("#influencerName").prop("disabled", true);
    this.contentinfluencer = this.listener.eventinfluencer;
    this.influencerModel = client.influencer_name;
    this.influencerModals.show();    
  }
  deleteInfluencerkeyfunc(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let dataform = {
      influencer_name: this.influencerModel
    };
    this.http
      .post<UserResponse>(
        environment.apiInfluencer+"delete-influencer-key/",
        dataform,
        this.options
      )
      .subscribe(
        (result: any) => {
          this.parent.progressRef.complete();
          this.influencerModals.hide();
          this.loadInfluencer();
          this.dataold = []
        },
        (err: any) => {
          this.alerts = [];
          this.alerts.push({
            type: "danger",
            msg: "snap, error while saving data.",
            timeout: 3000
          });
          this.parent.progressRef.complete();
          this.influencerModals.hide();
        }
      );
  }
}
