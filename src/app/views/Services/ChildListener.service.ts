import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ChildListenerService {
  private notify = new Subject<any>();
  notifyObservable$ = this.notify.asObservable();
  constructor(
    private http: HttpClient
  ){}

  public notifyOther(data: any) {
    if (data) {
      this.notify.next(data);
    }
  }
}
