import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { HttpClient} from '@angular/common/http';
@Component({
  selector: 'app-ArticlesModule',
  templateUrl: './ArticlesModule.component.html',
  styleUrls: ['./ArticlesModule.component.scss']
})
export class ArticlesModuleComponent implements OnInit {

  @ViewChild('articlesModal') public articlesModal: ModalDirective;
  @ViewChild('articleDetailContainer') public articleDetailContainer;
  @ViewChild('articleDetailContainerMaps') public articleDetailContainerMaps;

  articlesList: any = [];
  pageSize: number = 10;
  currentPage: number = 1;
  category_set:any;
  category_id:any;
  user_media_type_id:any;
  media:any;
  start:any;
  end:any;
  tone:any;
  timeFrame:any = 2;
  nameuse:any;
  pagecurrentuse:any;
  constructor(private http: HttpClient) { }

  showArticleDetail(article) {
    this.articleDetailContainerMaps.loadArticlesmap(article, false);
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.loadArticlesmaps(this.nameuse,this.start, this.end ,event.page)
  }

   loadArticlesmaps(namakota,start,end,page) {
    this.nameuse = namakota.replace('KABUPATEN', '')
    this.start = start;
    this.end = end;
    this.pagecurrentuse = page - 1 
    let body = new URLSearchParams();
      body.set('start_date', start);
      body.set('end_date', end);
      body.set('maxSize', '10');
      body.set('order_by', 'datee');
      body.set('order', 'desc');
      body.set('geoLoc', this.nameuse);
      body.set('page' , this.pagecurrentuse);
    interface UserResponse {
      data: Object;
    }
    this.http.get<UserResponse>('./apidigivla/articles-by-geo/?'+body)
      .subscribe((result: any) => {
        this.articlesList = result;
        this.articlesModal.show();
      }, (err: any) => {

      });
  }
  ngOnInit() {
  }


}
