import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubCategoryComponent } from './SubCategory.component';
import { SubCategoryRoutingModule } from './SubCategory-routing.module';

import { FormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  imports: [
    CommonModule,
    SubCategoryRoutingModule,
    FormsModule,    
    AlertModule.forRoot(),
    ReactiveFormsModule,
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  declarations: [SubCategoryComponent]
})
export class SubCategoryModule { }
