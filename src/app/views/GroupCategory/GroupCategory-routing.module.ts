import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';

import { GroupCategoryComponent } from './GroupCategory.component';

const routes: Routes = [
  {
    path: '',
    component: GroupCategoryComponent,
    data: {
      title: 'Group Category'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupCategoryRoutingModule {}
