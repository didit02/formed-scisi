import { Component, OnInit,OnDestroy,ViewChild,Inject,TemplateRef } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {DefaultLayoutComponent} from '../../containers/default-layout/default-layout.component'
import { moment } from 'ngx-bootstrap/chronos/test/chain';

import { Subscription } from 'rxjs/Subscription';
import { ChildListenerService } from '../../views/Services/ChildListener.service';
import {environment} from '../../../environments/environment'
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-NewsClipping',
  templateUrl: './NewsClipping.component.html',
  styleUrls: ['./NewsClipping.component.scss']
})
export class NewsClippingComponent implements OnInit, OnDestroy {
  url=environment.apiUrl;
  article_idMentionModel;
  category_idMentionModel;
  toneModel=0;
  summaryModel;
  private subscription: Subscription;
  checkedAllModel:boolean=false;
  editingList:any=[];
  objectEditing;
  articleChecked;
  emailModal;
  bodyemailModal :any = '';
  deleteShow:boolean=false;
  request:boolean = false;
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Token ' + localStorage.getItem('token') });
  options = { headers: this.headers };

  alerts;

  pageSize:number=10;
  currentPage:number =1;
  searchModel;
  modalRef: BsModalRef;
  isASC=false;


  groupMediaOption: any[] = [];
  subMediaOption: any[] = [];

  constructor(@Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent,  private modalService: BsModalService,private http: HttpClient,private childListener: ChildListenerService) { }
  @ViewChild('searchModalContainer') public searchModalContainer;
  @ViewChild('pushEmailModal') public pushEmailModal;
  @ViewChild('pushrequestlModal') public pushrequestlModal;

  sortingHandler(field){
    var sort;
    if (this.isASC == false){
      this.isASC = true;
      sort = 'asc';
    }else{
      this.isASC = false;
      sort = 'desc'
    }

    this.loadEditingData(1,field,sort);
  }


  pageChanged(event: any): void {
    this.currentPage= event.page;
    this.loadEditingData(event.page,'datee','desc');
  }


  onSizeChange(){
    this.loadEditingData(this.currentPage,'datee','desc');
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }


  decline(): void {
    this.modalRef.hide();
  }

  doDeleteArticle(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    var articleArr = this.articleChecked.map(o => {
      return {'article_id':o.article_id};
    });

    this.http.post<UserResponse>(this.parent.url +'user/article/delete',articleArr,this.options)
    .subscribe((result: any) => {
      setTimeout(()=>{ this.loadPromise(this.currentPage,'datee','desc').then(result => {
        this.editingList = result;
        if(this.checkedAllModel==true){
          this.onCheckedAllChange(this.editingList);
        }
        this.checkAnyChecked();
        this.alerts=[];
        this.alerts.push({'type':'success','msg':'Article has been deleted','timeout':3000})
        this.articleChecked=[];
        this.editingList.data.forEach(function(element) { element.checked = false; });
        this.deleteShow=false;
        this.modalRef.hide();
        this.parent.progressRef.complete();
        })
      }, 3000)
    },(err:any)=>{
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000})
      this.articleChecked=[];
      this.editingList.data.forEach(function(element) { element.checked = false; });
      this.deleteShow=false;
      this.modalRef.hide();
      this.parent.progressRef.complete();
    });
  }

  loadPromise(page,field,sort){
    return new Promise((resolve, reject) => {
      interface UserResponse {
        data: Object;
      }

      let editingParam={
        "category_set":this.parent.groupCategoryModel,
        "category_id":this.parent.subCategoryModel=='All Sub Categroy'?'all':this.parent.subCategoryModel,
        "user_media_type_id":this.parent.groupMediaModel,
        "media_id": this.parent.subMediaModel,
        "term": this.searchModel,
        "start_date": (moment(this.parent.periodFromModel).format('YYYY-MM-DD')).toString(),
        "end_date": (moment(this.parent.periodEndModel).format('YYYY-MM-DD')).toString(),
        "maxSize": this.pageSize,
        "page": page-1,
        "order_by": field,
        "order": sort
      };

      this.http.post<UserResponse>(this.parent.url +'user/editing/',editingParam,this.options)
      .subscribe((result: any) => {
        result.data.forEach(function(element) { element.checked = false; });
        resolve(result)
      },(err:any)=>{
        this.alerts=[];
        this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000})
        reject(this.alerts);
      });
   });
  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

  DocHandler(){
    this.parent.progressRef.start();
    var articleArr = this.articleChecked.map(o => {
      return o.article_id;
    });
    var url = './restapi/?stat=1&article='+ articleArr.join() +'&formate=0&logo_head='+ localStorage.getItem('logo') +'&client_id='+ localStorage.getItem('client');
    window.open(url);
    this.parent.progressRef.complete();
  }

  textHandler(){
    var articleArr = this.articleChecked.map(o => {
      return o.article_id;
    });

    var url = './restapi/?stat=2&article='+ articleArr.join() +'&formate=1&logo_head='+ localStorage.getItem('logo') +'&client_id='+ localStorage.getItem('client');
    window.open(url);
  }

  scanHandler(){
    var articleArr = this.articleChecked.map(o => {
      return o.article_id;
    });
    var url = './restapi/?stat=1&article='+ articleArr.join() +'&formate=1&logo_head='+ localStorage.getItem('logo') +'&client_id='+ localStorage.getItem('client');
    window.open(url);
  }

  emailHandler(){
    this.pushEmailModal.show();
  }

  doPushEmail(){
    var emailParam = {"email":this.emailModal,"data":this.articleChecked}
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    this.http.post<UserResponse>(this.parent.url +'user/editing/send-mail',emailParam,this.options)
    .subscribe((result: any) => {
      this.pushEmailModal.hide();
      this.alerts=[];
      this.alerts.push({'type':'success','msg':'Email has been sent','timeout':3000})
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.pushEmailModal.hide();
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000})
      this.parent.progressRef.complete();
    });
  }

  loadEditingData(page,field,sort){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }

    let editingParam={
      "category_set":this.parent.groupCategoryModel,
      "category_id":this.parent.subCategoryModel=='All Sub Categroy'?'all':this.parent.subCategoryModel,
      "user_media_type_id":this.parent.groupMediaModel,
      "media_id": this.parent.subMediaModel,
      "term": this.searchModel,
      "start_date": (moment(this.parent.periodFromModel).format('YYYY-MM-DD')).toString(),
      "end_date": (moment(this.parent.periodEndModel).format('YYYY-MM-DD')).toString(),
      "maxSize": this.pageSize,
      "page": page-1,
      "order_by": field,
      "order":sort
    };
    this.http.post<UserResponse>(this.parent.url +'user/editing/',editingParam,this.options)
    .subscribe((result: any) => {
      result.data.forEach(function(element) { element.checked = false; });
      
      this.editingList = result;
      if(this.checkedAllModel==true){
        this.onCheckedAllChange(this.editingList);
      }
      this.checkAnyChecked();
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000})
      this.parent.progressRef.complete();
    });
  }

  onCheckedChange(client:any,index:any){
    if (client.checked==true){
      if (localStorage.getItem('client') === 'ganna'){
        this.request = true;
      }else{
        this.request = false;
      }
      this.articleChecked.push({"article_id":client.article_id,"category_id":client.category_id});
      this.deleteShow = true;
    } else{
      let index = this.articleChecked.findIndex(res => res.article_id === client.article_id);
      this.articleChecked.splice(index,1);
      if (this.articleChecked.length<1){
        this.deleteShow=false;
        this.request = false;
      }
    }
  }

  checkAnyChecked(){
    if(this.articleChecked.length > 0){      
      for(let i=0;i<this.editingList.data.length;i++){
        for(let j=0;j<this.articleChecked.length;j++){
            if (this.editingList.data[i].article_id == this.articleChecked[j].article_id){
              this.editingList.data[i].checked = true;;
            }
        }
      }
    }
  }

  onCheckedAllChange(data){    
    if (this.checkedAllModel==true){            
      this.editingList.data.forEach(function(element) { element.checked = true; });
      this.articleChecked=[];
      if (localStorage.getItem('client') === 'ganna'){
        this.request = true;
      }else{
        this.request = false;
      }
      for(let i=0;i< this.editingList.data.length;i++){
        this.articleChecked.push({"article_id":this.editingList.data[i].article_id,"category_id":this.editingList.data[i].category_id})
      }
      this.deleteShow = true;
    } else{
      this.request = false;
      this.editingList.data.forEach(function(element) { element.checked = false; });
      this.articleChecked=[];
      this.deleteShow = false;
    }
  }

  showSearchModal(article:any){
    var highlightedWord;
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
     let param = {'article_id': article.article_id}

    this.http.post<UserResponse>(this.parent.url +'user/keywords-by-article-id/',param,this.options)
    .subscribe((result: any) => {      
      this.parent.progressRef.complete();
      var res =[];
      res = result.data;
      var argumenta =[];
      var args = res.join().split(',');
      for (let i = 0; i< args.length;i++){
        var argsExp = args[i].split('" "')
        if (argsExp.length>1){
          for(let j=0;j<argsExp.length;j++){
            var re = new RegExp('"', 'gi');
            var rplc = argsExp[j].replace(re, "");
            argumenta.push(rplc);
          }
        }else{
          var re = new RegExp('"', 'gi');
          var rplc = args[i].replace(re, "");
          argumenta.push(rplc);
        }
      }
      highlightedWord = argumenta.join();
      var re = new RegExp(',', 'gi');
      var highlightedWord = highlightedWord.replace(re, "||");

      if (article.summary==""||article.summary==undefined){
        this.searchModalContainer.contentModel = highlightedWord==''? this.highlighted(article.content,article.category_id) : this.highlighted(article.content,highlightedWord);
      this.searchModalContainer.summaryModel = highlightedWord==''? this.highlightedsummary(article.content,article.category_id) : this.highlightedsummary(article.content,highlightedWord);
      }else{
        this.searchModalContainer.contentModel = highlightedWord==''? this.highlighted(article.summary,article.category_id) : this.highlighted(article.summary,highlightedWord);
      this.searchModalContainer.summaryModel = highlightedWord==''? this.highlightedsummary(article.summary,article.category_id) : this.highlightedsummary(article.summary,highlightedWord);
      }      
      

      this.searchModalContainer.filePdfModel = article.file_pdf;
      this.searchModalContainer.mediaExtensionModel = article.file_pdf.split('.')[1];
      this.searchModalContainer.categoriesList = article.categories.length==0?[article.category_id]:article.categories;
      this.searchModalContainer.articleArr = article;
      if (article.title_edit === '' || article.title_edit === undefined){
        this.searchModalContainer.titleModel = article.title;
      }else{
        this.searchModalContainer.titleModel = article.title_edit;
      }      this.searchModalContainer.dateModel = article.datee;
      this.searchModalContainer.mediaModel = article.media_name;
      this.searchModalContainer.pageDesc = {
        'print':false,
        'pdf_text':true,
        'doc':true,
        'save':true,
        'close':true,
        'scan_media':true,
        'text_print':true,
        'link':true
      };
      this.searchModalContainer.urlFileModel = "./restinput/media_tv/"+ article.file_pdf.split('-')[0] + "/" + article.file_pdf.split('-')[1] + "/" + article.file_pdf.split('-')[2] + "/" + article.file_pdf;
      this.searchModalContainer.searchModal.show();
    },(err:any)=>{
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, failed while loading data.','timeout':3000})
      this.parent.progressRef.complete();
    });
  }

  highlighted(value: any, args: any): any {
    let argsArray=[];
    let keyAray=[];

    if (args.includes('&&')) {
      argsArray = args.split('&&');
    }else if (args.includes('||')){
      argsArray = args.split('||');
    }else{
      argsArray.push(args);
    }
    for (let i=0; i < argsArray.length;i++){
      var re = new RegExp((argsArray[i].trimEnd()).trimStart(), 'gi');
      value = value.replace(re, "<mark>" + (argsArray[i].trimEnd()).trimStart() + "</mark>");
    }
    return value;
  }
  highlightedsummary(value: any, args: any): any {
    let argsArray=[];

    if (args.includes('&&')) {
      argsArray = args.split('&&');
    }else if (args.includes('||')){
      argsArray = args.split('||');
    }else{
      argsArray.push(args);
    }
    return value;
  }

  clickSearch(){
    this.currentPage=1;
    this.loadEditingData(1,'datee','desc');
  }

  
  getMediaGroup() {
    return new Promise((resolve, reject) => {
      interface UserResponse {
        data: Object;
      }
      this.http.get<UserResponse>(this.url + 'user/medias/', this.options)
        .subscribe((result: any) => {
          this.groupMediaOption = result.results;
          this.parent.groupMediaModel = this.groupMediaOption[0].user_media_type_id;
          resolve(this.parent.groupMediaModel);

        });
    });
  }

  getSubMedias() {
    interface UserResponse {
      data: Object;
    }
    this.http.get<UserResponse>(this.url + 'user/submedias/' + this.parent.groupMediaModel, this.options)
      .subscribe((result: any) => {
        this.subMediaOption = result.results;
        if (this.subMediaOption.length < 1) {
          if (this.parent.groupMediaModel == '0') {
            this.subMediaOption.unshift({ "media_id": 0, "media_name": "All Sub Media", "media_type_id": 1, "circulation": 35700, "rate_bw": 117000, "rate_fc": 195000, "language": "IND", "statuse": "A", "usere": "1", "pc_name": "", "input_date": "2012-11-22T11:05:42Z", "tier": 2 });
          } else {
            this.subMediaOption.unshift({ "media_id": 0, "media_name": "No Sub Media", "media_type_id": 1, "circulation": 35700, "rate_bw": 117000, "rate_fc": 195000, "language": "IND", "statuse": "A", "usere": "1", "pc_name": "", "input_date": "2012-11-22T11:05:42Z", "tier": 2 });
          }
        } else {
          this.subMediaOption.unshift({ "media_id": 0, "media_name": "All Sub Media", "media_type_id": 1, "circulation": 35700, "rate_bw": 117000, "rate_fc": 195000, "language": "IND", "statuse": "A", "usere": "1", "pc_name": "", "input_date": "2012-11-22T11:05:42Z", "tier": 2 });
        }
        this.parent.subMediaModel = this.subMediaOption[0].media_id;
      });
  }

  ngOnInit() {
    // console.log(localStorage.getItem('client'));
    this.articleChecked =[];
    this.loadEditingData(1,'datee','desc');
    this.getMediaGroup().then((res)=>{
      this.getSubMedias();
      this.loadEditingData(1,'datee','desc');
    })

    this.subscription = this.childListener.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('option') && res.option === 'call_child') {
        this.loadEditingData(1,'datee','desc');
      }else if (res.hasOwnProperty('option') && res.option === 'save_article') {
        for(let i=0;i<this.editingList.data.length;i++){
          if(this.editingList.data[i].article_id==res.value.article_id){
            this.editingList.data[i].categories = res.value.categories;
          }
        }
      }
    });
  }
  translateHandler(){
    this.pushrequestlModal.show()
  }
  doPushRequest(){
    let dataemail = [];
    // dataemail.push(this.emailModal.split(','))    
    var emailParam = {"email":this.emailModal,"body": this.bodyemailModal,"data":this.articleChecked}    
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    this.http.post<UserResponse>(this.parent.url +'user/editing/send-mail-translate',emailParam,this.options)
    .subscribe((result: any) => {
      this.pushrequestlModal.hide();
      this.alerts=[];
      this.alerts.push({'type':'success','msg':'Email has been sent','timeout':3000})
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.pushrequestlModal.hide();
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000})
      this.parent.progressRef.complete();
    });
  }
  closemodalrequest(){
    this.pushrequestlModal.hide()
    this.emailModal = '';
    }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  
}
