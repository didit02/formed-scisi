import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupMediaRoutingModule } from './GroupMedia-routing.module';
import { GroupMediaComponent } from './GroupMedia.component';
import { FormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AccordionModule } from 'ngx-bootstrap/accordion';


@NgModule({
  imports: [
    CommonModule,
    GroupMediaRoutingModule,
    FormsModule,    
    AlertModule.forRoot(),
    ReactiveFormsModule,
    ModalModule.forRoot(),
    AccordionModule.forRoot()
  ],
  declarations: [GroupMediaComponent]
})
export class GroupMediaModule { }
