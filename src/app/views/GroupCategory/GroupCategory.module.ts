import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupCategoryComponent } from './GroupCategory.component';
import { GroupCategoryRoutingModule } from './GroupCategory-routing.module';

import { FormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AccordionModule } from 'ngx-bootstrap/accordion';
@NgModule({
  imports: [
    CommonModule,
    GroupCategoryRoutingModule,
    FormsModule,    
    AlertModule.forRoot(),
    ReactiveFormsModule,
    ModalModule.forRoot(),
    AccordionModule.forRoot()
  ],
  declarations: [GroupCategoryComponent]
})
export class GroupCategoryModule { }
