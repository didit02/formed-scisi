import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import * as wordcloud from 'highcharts/modules/wordcloud';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ArticleDetailsModule } from '../ArticleDetails/ArticleDetails.module'
import { ModalModule } from 'ngx-bootstrap/modal';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TrendsRoutingModule } from "./Trends-routing.module";
import { TrendsComponent } from "./Trends.component";
@NgModule({
  imports: [FormsModule,
    CommonModule,
    ChartsModule, 
    ChartModule, 
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    ArticleDetailsModule,  TrendsRoutingModule,AlertModule],
  declarations: [TrendsComponent],
  providers: [
    { provide: HIGHCHARTS_MODULES, useFactory: () => [wordcloud] }
  ]
})
export class TrendsModule {}
