import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { GroupMediaComponent } from './GroupMedia.component';

const routes: Routes = [
  {
    path: '',
    component: GroupMediaComponent,
    data: {
      title: 'Group Media'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupMediaRoutingModule {}
