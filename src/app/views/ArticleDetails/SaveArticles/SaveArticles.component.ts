import { Component, OnInit,ViewChild,Output,EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { NgProgress,NgProgressRef } from '@ngx-progressbar/core'
import {environment} from '../../../../environments/environment'
import { moment } from 'ngx-bootstrap/chronos/test/chain';
@Component({
  selector: 'app-SaveArticles',
  templateUrl: './SaveArticles.component.html',
  styleUrls: ['./SaveArticles.component.scss']
})
export class SaveArticlesComponent implements OnInit {
  url:any=environment.apiUrl;
  categoryList:any;
  toneModel:number=0;
  articleArr:any;
  categoryChecked:any;

  @ViewChild('saveArticleModal') public saveArticleModal: ModalDirective;
  constructor(private http: HttpClient,public progress: NgProgress) {}
  progressRef: NgProgressRef;
  @Output() onSave: EventEmitter<any> = new EventEmitter();


  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Token ' + localStorage.getItem('token') });
  options = { headers: this.headers };

  saveHandler(){
    this.progressRef.start();

    interface UserResponse {
      data: Object;
    }
    
   
    this.categoryChecked = this.categoryList.filter( function(item){return (item.checked==true);} );
    this.categoryChecked = this.categoryChecked.map(o => {
      return  o.category_id ;
    });

    let paramSave={
      article_id:this.articleArr.article_id,
      category_ids:this.categoryChecked,
      datee:(moment(this.articleArr.datee).format('YYYY-MM-DD')).toString(),
      media_id:this.articleArr.media_id,
      tone:this.toneModel,
      advalue_fc:this.articleArr.rate_fc,
      circulation:this.articleArr.circulation,
      advalue_bw:this.articleArr.rate_bw
    }
    this.http.post<UserResponse>(this.url +'user/article/save',paramSave,this.options)
    .subscribe((result: any) => {
      this.progressRef.complete();
      if(result.code =='200'){
        this.onSave.emit(result);                  
      }else{
        this.onSave.emit('failed');        
      }
      this.saveArticleModal.hide();      
    });  
  }
  ngOnInit() {
    this.progressRef = this.progress.ref();      
  }

}
