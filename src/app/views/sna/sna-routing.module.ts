import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SnaComponent } from "./sna.component";
const routes: Routes = [
  {
    path: '',
    component: SnaComponent,
    data: {
      title: 'Sna'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SnaRoutingModule { }
