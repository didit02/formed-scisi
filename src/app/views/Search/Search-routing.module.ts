import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { SearchComponent } from './Search.component';

const routes: Routes = [
  {
    path: '',
    component: SearchComponent,
    data: {
      title: 'Search'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule {}
