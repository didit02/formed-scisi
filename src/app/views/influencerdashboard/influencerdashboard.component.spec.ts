import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfluencerdashboardComponent } from './influencerdashboard.component';

describe('InfluencerdashboardComponent', () => {
  let component: InfluencerdashboardComponent;
  let fixture: ComponentFixture<InfluencerdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfluencerdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfluencerdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
