import { DefaultLayoutComponent } from "./containers/default-layout/default-layout.component";

// constructor( private parent: DefaultLayoutComponent){}

export const navReader = [
  {
    name: "Dashboard",
    url: "/dashboard",
    icon: "icon-speedometer"
  },
  {
    name: "Spokesperson",
    url: "/influencerdashboard",
    icon: "icon-microphone"
  },
  {
    name: "Editing",
    url: "/editing",
    icon: "cui-note icons"
  },
  {
    name: "News Clipping",
    url: "/newsclipping",
    icon: "cui-puzzle"
  },
  {
    name: "Search",
    url: "/search",
    icon: "cui-magnifying-glass"
  },
  {
    name: "Connect",
    url: "/connect",
    icon: "icon-people"
  }
  //   {
  //     name: 'Configurations',
  //     url: '/configurations',
  //     icon: 'cui-settings',
  //     children: [
  //       {
  //         name: 'Group Media',
  //         url: '/groupmedia',
  //         icon: 'icon-feed'
  //       },
  //       {
  //         name: 'Group Category',
  //         url: '/groupcategory',
  //         icon: 'icon-notebook'
  //       },
  //       {
  //         name: 'Sub Category',
  //         url: '/subcategory',
  //         icon: 'icon-tag'
  //       },
  //       {
  //         name: 'Influencer',
  //         url: '/influencer',
  //         icon: 'icon-tag'
  //       }
  //     ]
  //   }
];
