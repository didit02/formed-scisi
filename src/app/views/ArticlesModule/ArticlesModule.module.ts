import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticlesModuleComponent } from './ArticlesModule.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FormsModule } from '@angular/forms';
import { ArticleDetailsModule } from '../ArticleDetails/ArticleDetails.module'

@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    FormsModule,    
    PaginationModule.forRoot(),
    ArticleDetailsModule
  ],
  declarations: [ArticlesModuleComponent],
  exports: [
    ArticlesModuleComponent
  ]
})
export class ArticlesModuleModule { }
