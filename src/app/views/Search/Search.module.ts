import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchComponent } from './Search.component';
import { ArticleDetailsModule } from '../ArticleDetails/ArticleDetails.module'
// import { SaveArticlesComponent } from '../ArticleDetails/SaveArticles/SaveArticles.component';

import { SearchRoutingModule } from './Search-routing.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgxLoadingModule } from 'ngx-loading';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,    
    AlertModule.forRoot(),
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    NgProgressModule.forRoot(),
    SearchRoutingModule,
    ArticleDetailsModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [
    SearchComponent,    
    // ArticleDetailsComponent,
    // SaveArticlesComponent
  ]
})
export class SearchModule { }
