import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { AlertModule } from 'ngx-bootstrap/alert';

import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import * as more from 'highcharts/highcharts-more.src';
import * as exporting from 'highcharts/modules/exporting.src';
import * as offlineExporting from 'highcharts/modules/offline-exporting.src';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

// import { HighchartsChartModule } from 'highcharts-angular';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ArticleDetailsModule } from '../ArticleDetails/ArticleDetails.module'
import * as highstock from 'highcharts/modules/stock.src';


@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    DashboardRoutingModule,
    ChartsModule,
    BsDropdownModule,
    CarouselModule.forRoot(),
    ButtonsModule.forRoot(),
    AlertModule.forRoot(),
    ChartModule,
    // HighchartsChartModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    ArticleDetailsModule
  ],
  declarations: [ DashboardComponent ], 
  providers: [
    { provide: HIGHCHARTS_MODULES, useFactory: () => [ more, exporting,offlineExporting,highstock ] } // add as factory to your providers
  ] 
})
export class DashboardModule { }
