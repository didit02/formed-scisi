import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JournalistConnectComponent } from './JournalistConnect.component';

const routes: Routes = [
  {
    path: '',
    component: JournalistConnectComponent,
    data: {
      title: 'Connect'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JournalistConnectRoutingModule {}
