import { Component, OnInit, Inject, ViewChild } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Options } from "selenium-webdriver/firefox";
import { DefaultLayoutComponent } from "../../containers/default-layout/default-layout.component";
import { environment } from "../../../environments/environment";
import { from } from "rxjs";

import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { BsModalService } from "ngx-bootstrap/modal";

@Component({
  selector: "app-dashboard",
  templateUrl: "login.component.html"
})
export class LoginComponent {
  ModelEmail: string = "";
  ModelPassword: string = "";
  clients: any;
  showSelected = false;
  msgerr;
  url = environment.apiUrl;
  modalRef: BsModalRef;
  imagelogin:any;

  @ViewChild("popUpHandlerLogin") public popUpHandlerLogin; //add ahmadk

  public loading = false;
  constructor(
    private http: HttpClient,
    private router: Router,
    private BsmodalService: BsModalService
  ) {}

  login() {
    this.loading = true;
    let fd = {
      username: this.ModelEmail,
      password: this.ModelPassword
    };

    interface UserResponse {
      data: Object;
    }
    this.http.post<UserResponse>(this.url + "login/", fd).subscribe(
      result => {
        this.clients = result;

        if (this.clients.code != "401") {
          localStorage.setItem("user", this.clients.usr_comp_login);
          localStorage.setItem("token", this.clients.token);
          localStorage.setItem("client", this.clients.client_id);
          localStorage.setItem("logo", this.clients.comp_icon);
          // for to admin
          localStorage.setItem("levelmenu", this.clients.usr_comp_level);
          this.router.navigateByUrl("/dashboard");
        } else {
          this.loading = false;
          // alert(this.clients.message)
          this.msgerr = this.clients.message; //add ahmadk
          this.popUpHandlerLogin.show();

          // this.showSelected  = true;
        }
      },
      error => {
        this.loading = false;
        // this.msgerr = JSON.stringify(error.json());

        this.msgerr = "Please Input Correctly the login forms.."; //add ahmadk
        this.popUpHandlerLogin.show();
      }
    );
  }

  ngOnInit(): void {    
    if (window.location.host === 'scisi.mediamonitoring.id'){
      this.imagelogin = '/assets/img/brand/sucofindo1.png'
    }else{
      this.imagelogin = 'assets/img/brand/igico.png'
    }
  }
}
