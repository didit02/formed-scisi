import { Component, OnInit,ViewChild,Inject,TemplateRef } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { NgProgress,NgProgressRef } from '@ngx-progressbar/core'
import {environment} from '../../../environments/environment'
import { DefaultLayoutComponent } from '../../containers/default-layout/default-layout.component'
import { ChildListenerService } from '../../views/Services/ChildListener.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

@Component({
  selector: 'app-GroupMedia',
  templateUrl: './GroupMedia.component.html',
  styleUrls: ['./GroupMedia.component.scss']
})
export class GroupMediaComponent implements OnInit {
  url=environment.apiUrl;
  groupCategoryList:any=[];
  mediaCheckedArray:any=[];
  groupMediaDetailList:any=[];

  modalRef: BsModalRef;

  @ViewChild('groupMediaDetail') public groupMediaDetail;  
  @ViewChild('editNameGroupMedia') public editNameGroupMedia;  

  newGroupMedia:string;
  alerts=[];

  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Token ' + localStorage.getItem('token') });
  options = { headers: this.headers };

  constructor(@Inject(DefaultLayoutComponent) private parent: DefaultLayoutComponent, private modalService: BsModalService, private http: HttpClient,private childListener: ChildListenerService) { }

  oneAtATime: boolean = true;
  message: string;
  checkedAllModel;
  pointerArray;
  editGroupMedia;
  detailsMediaList=[];

  addNewGroupMedia(){
   this.parent.progressRef.start();   
   interface UserResponse {
     data: Object;
   }
   let param = {"user_media_type_name_def": this.newGroupMedia}
   this.http.post<UserResponse>(this.url + 'user/media-group/create',param,this.options)
   .subscribe((result: any) => {
     this.groupCategoryList.push(result.data);
     this.newGroupMedia ='';
     this.alerts=[];
     this.alerts.push({'type':'success','msg':'Group Media has been created','timeout':3000}) 
     
     this.parent.progressRef.complete();
   },(err:any)=>{
     this.alerts=[];
     this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
     this.parent.progressRef.complete();
   }); 
  }

  openModal(template: TemplateRef<any>,client) {
    this.pointerArray = client;
    // console.log(this.pointerArray);
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});

  }

  doDeleteGroupMedia(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let param = {"user_media_type_id": this.pointerArray.user_media_type_id}
    this.http.post<UserResponse>(this.url + 'user/media-group/delete',param,this.options)
    .subscribe((result: any) => {
      for (let i=0;i<this.groupCategoryList.length;i++){
        if (this.groupCategoryList[i].user_media_type_id== result.data.user_media_type_id){
          this.groupCategoryList.splice(i,1);
        }
      }
      this.modalRef.hide(); 
      this.alerts=[];
      this.alerts.push({'type':'success','msg':'Group Media has been deleted','timeout':3000}) 
      
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.modalRef.hide(); 
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
      this.parent.progressRef.complete();
    }); 
  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

  decline(): void {
    this.modalRef.hide();
  }

  updateGroupMedia(client){
    this.pointerArray = client;
    this.editGroupMedia = this.pointerArray.user_media_type_name_def;
    this.editNameGroupMedia.show();
  }
  doUpdateGroupMedia(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    let param = {"user_media_type_id": this.pointerArray.user_media_type_id, "user_media_type_name_def":this.editGroupMedia}
    this.http.post<UserResponse>(this.url + 'user/media-group/update',param,this.options)
    .subscribe((result: any) => {
      for (let i=0;i<this.groupCategoryList.length;i++){
        if (this.groupCategoryList[i].user_media_type_id== result.data.user_media_type_id){
          this.groupCategoryList[i].user_media_type_name_def = result.data.user_media_type_name_def;
        }
      }
      this.editNameGroupMedia.hide();
      this.editGroupMedia ='';
      this.alerts=[];
      this.alerts.push({'type':'success','msg':'Group Media has been updated','timeout':3000}) 
      
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.editNameGroupMedia.hide();
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while saving data.','timeout':3000}) 
      this.parent.progressRef.complete();
    }); 
  }

  showDetails(client){
    this.parent.progressRef.start();
    this.pointerArray = client;
    
    interface UserResponse {
      data: Object;
    }
    let param = {"user_media_type_id": client.user_media_type_id}
    this.http.post<UserResponse>(this.url + 'user/media-groups/',param,this.options)
    .subscribe((result: any) => {
      result.data.forEach(function(element) { element.checkedAll = false; }); 
      this.groupMediaDetailList =[];
      this.groupMediaDetailList = result;    
      
      this.parent.progressRef.complete();
      this.groupMediaDetail.show();
    },(err:any)=>{
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
      this.parent.progressRef.complete();
    });     
  }
  
  checkallAllSubMedia(checked,index){
    if (checked==true){
      this.groupMediaDetailList.data[index].media_list.forEach(function(element) { element.chosen=true }); 
      // this.detailsMediaList = this.groupMediaDetailList.map(o => {
      //   return {'media_id':o.media_id,'chosen':o.chosen};
      // });
    } else{
      this.groupMediaDetailList.data[index].media_list.forEach(function(element) { element.chosen=false }); 
    }  
  }

  loadGroupMedia(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }
    this.http.get<UserResponse>(this.url + 'user/medias/',this.options)
    .subscribe((result: any) => {
      this.groupCategoryList = result.results;    
      // console.log(JSON.stringify(result));
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while loading data.','timeout':3000}) 
      this.parent.progressRef.complete();
    }); 
  }

  onCheckedAllChange(){
    if (this.checkedAllModel==true){      
      for(let i =0; i < this.groupMediaDetailList.data.length; i++){        
        this.groupMediaDetailList.data[i].media_list.forEach(function(element) { element.chosen=true });
        for(let j=0;j< this.groupMediaDetailList.data[i].media_list.length;j++){
          this.detailsMediaList.push({'media_id':this.groupMediaDetailList.data[i].media_list[j].media_id,'chosen':this.groupMediaDetailList.data[i].media_list[j].chosen})
        }                 
      }            
    } else{
      for(let i =0; i < this.groupMediaDetailList.data.length; i++){
        this.groupMediaDetailList.data[i].media_list.forEach(function(element) { element.chosen=false }); 
      }
      this.mediaCheckedArray=[];      
    }
  }

  doSaveSubMedia(){
    this.parent.progressRef.start();
    interface UserResponse {
      data: Object;
    }

    var mediaList=[]; 
    for (let i =0; i < this.groupMediaDetailList.data.length; i++){      
      for (let j =0; j < this.groupMediaDetailList.data[i].media_list.length; j++){         
        mediaList.push({'media_id': this.groupMediaDetailList.data[i].media_list[j].media_id,'chosen':this.groupMediaDetailList.data[i].media_list[j].chosen});
      }
    }    

    var param = {
      'user_media_type_id' : this.pointerArray.user_media_type_id,
      'media_list' : mediaList
    };
    

    this.http.post<UserResponse>(this.url + 'user/media-chosen/update',param,this.options)
    .subscribe((result: any) => {
      this.groupMediaDetail.hide();
      this.detailsMediaList =[];
      this.alerts=[];
      this.alerts.push({'type':'success','msg':'Group Media has been updated','timeout':3000}) 
      
      this.parent.progressRef.complete();
    },(err:any)=>{
      this.groupMediaDetail.hide();
      this.alerts=[];
      this.alerts.push({'type':'danger','msg':'snap, error while saving data.','timeout':3000}) 
      this.parent.progressRef.complete();
    }); 
  }

  onCheckedChange(details,index){
    if (details.chosen==true){
      this.detailsMediaList.push({'media_id':details.media_id,'chosen':details.chosen});
    }else{
      for (let i = 0; i< this.detailsMediaList.length;i++){
        if (this.detailsMediaList[i].media_id == details.media_id){
          this.detailsMediaList.splice(i,1);
        }else{
          this.detailsMediaList.push({'media_id':details.media_id,'chosen':details.chosen});
        }
      }
    }
  }
  ngOnInit() {
    this.loadGroupMedia();
  }
}
